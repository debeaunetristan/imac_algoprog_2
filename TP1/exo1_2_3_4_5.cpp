#include <iostream>
#include <vector>
#include <math.h>
#include "exo1_2_3_4_5.h"

using namespace std;


// prototypage des fonctions
int power (int value, int n);
int fibo(int n);
int search (int value, int array[], int size);
void allEvens (int* evens, int array[], int evenSize, int arraySize);
bool mandelbrot(vec2 z, vec2 point, int n);


vec2 prod(vec2 z1, vec2 z2);
vec2 somme(vec2 z1, vec2 z2);
float norme (vec2 z);


// la fonction main
int main(){

    // TEST fibo
    //cout << fibo(10) << endl;

    int array [5] = {1,2,3,4,5};
    int evens [5] = {0,0,0,0,0};

    allEvens(evens,array,0,5);

    for (int i =0; i<5; i++){
        cout << evens[i] << endl<< endl<< endl;
    }

    // TEST search
    cout << search(10, array, 5) << endl;

    // TEST allEvens


    // TEST Mandelbrot

    vec2 z;
    z.x = 0;
    z.y = 0;

     vec2 point;
    point.x = 9;
    point.y = 7;
    
    
    if (mandelbrot(z, point, 15)) cout << "///////" << " vrai" << endl;
    if (!mandelbrot(z, point, 15)) cout << "///////" << " faux" << endl;

    return 0;
}

int power (int value, int n){
    if(n==0) return 1;
    if (n==1) return value;
    return value*power(value,n-1);
}


int fibo(int n){
    if (n==1) return 1;
    if (n==0) return 0;
    return fibo(n-1) + fibo(n-2);

}

int search (int value, int array[], int size){
    if (size == 0) return size-1;
    if (array[size]==value) return size;
    return search (value, array, size-1);
}

void allEvens (int* evens, int array[], int evenSize, int arraySize){
    if (arraySize==0) return ;// fin du parcour de array
    
    if (array[arraySize-1] %2 ==0) {// test si le nombre est pair
        evens[evenSize] = array[arraySize-1];
        return allEvens(evens, array, evenSize+1, arraySize-1); // appel; diminution de arraySize pour parcours, augmentation de evenSize pour remplissage
    }
    return allEvens(evens, array, evenSize, arraySize-1);

}

bool mandelbrot(vec2 z, vec2 point, int n){
    if (n == 0) return false;
    return (norme(z)<2.0) || mandelbrot(somme(point,prod(z,z)), point, n-1);
}




vec2 prod(vec2 z1, vec2 z2){
    vec2* zProd = new vec2;
    zProd->x = z1.x*z2.x - z1.y*z2.y;
    zProd->y = z1.x*z2.y + z1.y*z2.x;
    return *zProd;
}

vec2 somme(vec2 z1, vec2 z2){
    vec2* zSum = new vec2;
    zSum->x = z1.x+z2.x;
    zSum->y = z1.y+z2.y;
    return *zSum;
}

float norme (vec2 z){
    return sqrt(z.x*z.x + z.y*z.y);
}