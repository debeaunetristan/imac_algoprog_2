#include <iostream>

struct vec2{
    int x;
    int y;
};

int power (int value, int n);
int fibo(int n);
int search (int value, int array[], int size);
void allEvens (int* evens, int array[], int evenSize, int arraySize);
bool mandelbrot(vec2 z, vec2 point, int n);


vec2 prod(vec2 z1, vec2 z2);
vec2 somme(vec2 z1, vec2 z2);
float norme (vec2 z);