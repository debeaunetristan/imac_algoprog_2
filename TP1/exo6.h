#include <iostream>

///////////
/////////// STRUCTURES
///////////

struct DynaTableau{
    int nb;
    int tailleMax;
    int* tab;
};

struct Liste{
    int  val;
    Liste* next;
};

struct File{
    int container[10];
    int count = -0;
};

struct Pile{
    int container[10];
    int count = -0;
};

///////////
/////////// FONCTIONS
///////////

// ajoute
void ajouteDT(int valeur, DynaTableau* tab);
void ajouteLC(int valeur, Liste liste);

// recupere
int recupereDT (int n, DynaTableau dynTab);
int recupereLC (int n, Liste liste);

// cherche
int chercheLC(int valeur, Liste* liste);
int chercheDT(int valeur, DynaTableau dynTab);

// stocke
void stockeDT (int n, int valeur, DynaTableau* dynTab);
void stockeLC (int n, int valeur, Liste* liste);


// pousser
void pousser_file (File *f, int x);
bool pousser_pile (Pile *p, int x);



// retirer
int retirer_file(File *f);
int retirer_pile(Pile *p);






// Fonctions autres
void affiche(int* tab, int t);