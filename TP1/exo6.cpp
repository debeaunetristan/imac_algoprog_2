#include <iostream>
#include "exo6.h"
using namespace std;


// ajoute
void ajouteDT(int valeur, DynaTableau* tab);
void ajouteLC(int valeur, Liste liste);

// recupere
int recupereDT (int n, DynaTableau dynTab);
int recupereLC (int n, Liste liste);

// cherche
int chercheLC(int valeur, Liste* liste);
int chercheDT(int valeur, DynaTableau dynTab);

void affiche(int* tab, int t);

// la fonction main
int main(){
    
    // ------------ TEST POUR LES DT ----------------
    
    int tab[6];
    tab[0] = 1;
    tab[1] = 2;
    tab[2] = 3;
    tab[3] = 4;
    tab[4] = 5;
    DynaTableau test;
    test.nb = 5;
    test.tailleMax=6;
    test.tab = tab;

    affiche(tab, 5);

    ajouteDT(6, &test);
    affiche(test.tab, test.tailleMax);
    ajouteDT(7, &test);
    affiche(test.tab, test.tailleMax);


    // ------------ FIN DE TEST POUR LES DT ----------
    
    // ##############################################
    // ##############################################

    // ------------ TEST POUR LES LC ----------------

    
    // --------FIN DE TEST POUR LES LC --------------
    
    return 0;
}

// AJOUTE
void ajouteLC(int valeur, Liste* liste){
    Liste* suiv = new Liste;
    suiv->val = valeur;
    liste->next = suiv;
}

void ajouteDT(int valeur, DynaTableau* dynTab){
    if (dynTab->nb < dynTab->tailleMax) { // si reste de la place -> is OK
        dynTab->tab[dynTab->nb] = valeur;
        dynTab->nb ++;
        return;
    }

    int *newTab = new int[dynTab->tailleMax+1];

    // copie du tab dans le nouveau
    for (int i = 0; i<dynTab->tailleMax; i++){
        newTab[i] = dynTab->tab[i];
    }
    
    newTab[dynTab->tailleMax] = valeur; // ajout de la nouvelle valeur

    // MAJ de notre dynTab
    dynTab->nb ++;
    dynTab->tailleMax ++;
    dynTab->tab = newTab;
}



// recup
int recupereDT (int n, DynaTableau dynTab){
    return dynTab.tab[n];
}

int recupereLC (int n, Liste* liste){
    if (n == 0) return liste->val;
    return recupereLC(n-1, liste->next);
}




// CHERCHE

int chercheDT(int valeur, DynaTableau dynTab){
    for (int i =0; i<dynTab.nb; i++){
        if (dynTab.tab[i] == valeur) return i; // si on le trouve on renvoit son indexe
    }
    return -1; // si pas trouvé après la boucle -> il est pas là
}
int chercheLC(int valeur, Liste* liste){
    int count = 0;
    while (liste != nullptr){
        if(liste->val == valeur) return count;
        count ++;
        liste = liste->next;
    }
    return -1;
}

//STOCKE
void stockeDT (int n, int valeur, DynaTableau* dynTab){
    dynTab->tab[n] = valeur;
}

void stockeLC (int n, int valeur, Liste* liste){
    int count = 0;
    while (liste != nullptr){
        if(count == n){
            liste->val = valeur;
        }
        count ++;
        liste = liste->next;
    }
}


// POUSSER
void pousser_file (File *f, int x){
    
    if (f->count ==10) {
        for (int i =0; i<f->count-1 ; i++){
            f->container[i] = f->container[i+1];
        }
        f->container[f->count-1] = x;
    }

    else{
        f->container[f->count] = x;
        f->count ++;
    }
}

bool pousser_pile (Pile *p, int x){
    if (p->count ==10) return false;
    p->container[p->count] = x;
    p->count ++;
    return true;
}



// RETIRER
int retirer_file(File *f){
    int a = f->container[0];
    for (int i =f->count-1; i>0 ; i--){
            f->container[i-1] = f->container[i];
        }
    f->count --;

    return a;
}

int retirer_pile(Pile *p){
    int a = p->container[p->count];
    p->container[p->count-1] = 0; // pas nécessaire
    p->count = p->count-1;
    return a;
}





// fonctions autres
void affiche(int* tab, int t){
    for (int i=0; i<t; i++){
        cout << " / " << tab[i];
    }
    cout << endl;
}