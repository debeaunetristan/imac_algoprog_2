#include "exo1.h"
#include <time.h>
#include <stdio.h>
#include <iostream>

using namespace std;


int main (){
	
	/////// TEST DE L'INSERT //////////////
	/*
	Heap* test = new Heap;
	test->size = 10;
	int tab[256] = {20,18,13,12,8,10,11,2,5,3};
	test->heap = tab;

	test->print();
	test->insertHeapNode(17);
	test->print();
	*/


	/////// TEST DE L'HEAPIFY //////////////
	/*
	Heap* test2 = new Heap;
	int tab[256] = {2,6,4,8,56,24,30};
	test2 = buildHeap(tab,7);
	
	test2->print();
	*/

	/////// TEST DE L'HEAP SORT //////////////
	Heap* test2 = new Heap;
	int tab[256] = {2,6,4,8,56,24,30};
	test2 = buildHeap(tab,7);
	test2->print();
	int* tabSort = test2->heapSort();
	test2->print();
	display(tabSort, test2->size);

	return 0;
}
