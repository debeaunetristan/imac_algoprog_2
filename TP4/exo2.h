#include <time.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
using namespace std;

void swapInt(int* a, int* b);
void swapChar(char* a, char* b);
void display(int* tab, int taille);
void charFrequences(string data, int* frequences);
void insertHeapNode(char caractere, int frequence);






struct HuffmanNode {
    HuffmanNode* left = nullptr;
    HuffmanNode* right = nullptr;
    
    int value; // fréquence d'apparition
    char character; // caractère rep. le noeud
    string code; // code produit par le dictionnaire

    void printNode() {
        cout << "value : " << this->value << " // character : " << this->character << " // code : " << this->code << endl;
        
        if (this->left != nullptr) {
            cout << "enfant gauche" ;
            this->left->printNode();
        }
        if (this->right != nullptr) {
            cout << "enfant droit";
            this->right->printNode();
        }
    }
    

};

struct HuffmanHeap {
    int size = 0;
    HuffmanNode heap[256];

    int leftChild(int nodeIndex){
        if(nodeIndex*2 +1 < this->size){
            return 2*nodeIndex +1;
        }
        return -1;
    }

    int rightChild(int nodeIndex){
        if(nodeIndex*2 +2 < this->size){
            return 2*nodeIndex +2;
        }
        return -1;
    }

    void printHeap(){
        for (int i = 0; i<this->size; i++){
            cout << i << " ieme node : ";
            this->heap[i].printNode();
        }
        cout << endl;
    }



    void insertHeapNode(char caractere, int frequence){
        // initialisation des variables
        int i = this->size;
        HuffmanNode* node = new HuffmanNode;
        node->value = frequence;
        node->character = caractere;

        this->heap[i] = *node;
        this->size ++;
        
        // on parcout en partant de la fin
        // tant qu'on a pas fini l'abre (i>0) et que l'enfant plus grand que le parent
        while(i>0 && (this->heap[i].value > this->heap[(i-1)/2].value)){
            // j'effectue le swap
            swapInt(&heap[i].value, &heap[(i-1)/2].value);
            swapChar(&heap[i].character, &heap[(i-1)/2].character);
            // update de i ipour le parcour
            i = (i-1)/2;
        }
    }
} ;




void insertNode(HuffmanNode* tree, HuffmanNode* node){
    int valLeft = 0;
    int valRight = 0;
    if(node->value < 2*tree->value){
        if (tree->left == nullptr) tree->left = node;
        else {
            insertNode(tree->left, node);
            valLeft = tree->left->value;
        }
    }
    else if(node->value >= 2*tree->value){
        if (tree->right == nullptr) tree->right = node;
        else {
            insertNode(tree->right, node);
            valRight = tree->right->value;
        }    
    }

    // à la fin, tree n'est jamais une feuille, il doit contenir '\0' et la somme des valeurs des enfants
    tree->character = '\0';
    tree ->value = valLeft+valRight;

}



void huffmanHeap(int* frequences, HuffmanHeap* heap){
    for (int i=0; i<128; i++){
        if (frequences[i]!=0) heap->insertHeapNode((char)i, frequences[i]);
    }
}


void charFrequences(string data, int* frequences){
    int count = 0;
    int valASCII = 0;
    while (data[count] !='\0'){
        valASCII = (int) data[count];
        frequences[valASCII] ++;
        count++;
    }
}


void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}

void swapInt(int* a, int* b){
	int* swap = new int;
	*swap = *a;
	*a = *b;
	*b = *swap;
	delete swap;
}

void swapChar(char* a, char* b){
	char* swap = new char;
	*swap = *a;
	*a = *b;
	*b = *swap;
	delete swap;
}