#include <time.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
using namespace std;

void swap(int* a, int* b);
void display(int* tab, int taille);



struct Heap{
    int size;
    int* heap;

    int leftChild(int nodeIndex){
        if(nodeIndex*2 +1 < this->size){
            return 2*nodeIndex +1;
        }
        return -1;
    }

    int rightChild(int nodeIndex){
        if(nodeIndex*2 +2 < this->size){
            return 2*nodeIndex +2;
        }
        return -1;
    }

    void print(){
        display(this->heap, this->size);
    }


    void insertHeapNode(int value){
        // initialisation des variables
        int i = this->size;
        this->heap[i] = value;
        this->size ++;
        this->print();

        
        // on parcout en partant de la fin
        // tant qu'on a pas fini l'abre (i>0) et que l'enfant plus grand que le parent
        while(i>0 && (this->heap[i] > this->heap[(i-1)/2])){
            // j'effectue le swap
            swap(&heap[i], &heap[(i-1)/2]);
            // update de i ipour le parcour
            i = (i-1)/2;
        }
    }

    void heapify(int nodeIndex){
        int largest = nodeIndex;

        // récup de l'index du plus grand entre enfant gauche et racine
        if (leftChild(nodeIndex) !=-1 && this->heap[leftChild(nodeIndex)]>this->heap[largest]){
            largest = leftChild(nodeIndex);
        }

        // si enfant droit plus petit que le max, son indice devient le max
        if (rightChild(nodeIndex) !=-1 && this->heap[largest]<=this->heap[rightChild(nodeIndex)]){
            largest = rightChild(nodeIndex);
        }

        if (largest != nodeIndex){
            swap(&this->heap[nodeIndex], &this->heap[largest]);
            this->heapify(largest);
        }
    }

    void heapifyHeap(){
        int startId = this->size/2;
        for (int i = startId; i>=0; i--){
            this->heapify(i);
        }
    }

    int* heapSort(){
        int n = this->size;
        //parcour en partant de la fin en réduisant la taille de notre tas
        for (int i= this->size-1; i>=0; i--){
            // swap le last avec la racine
            swap(&this->heap[0],&this->heap[i]);
            // réduction de la taille
            this->size --;
            // réordonne le tas ?
            heapify(0);
        }
        this->size = n;
        return this->heap;
    }

};



/////////// FONCTIONS HORS DE LA CLASSE

Heap* buildHeap(int* numbers, int size);



Heap* buildHeap(int* numbers, int size){
	// on initialise le heap (désordonné a priori)
	Heap* res = new Heap;
	res->size = size;
	res->heap = numbers;

	// on va le mettre dans le bon ordre
	res->heapifyHeap();

	return res;
}

void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}

void swap(int* a, int* b){
	int* swap = new int;
	*swap = *a;
	*a = *b;
	*b = *swap;
	delete swap;
}




