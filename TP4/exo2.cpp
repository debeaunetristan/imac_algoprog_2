#include "exo2.h"
#include <time.h>
#include <stdio.h>
#include <iostream>

using namespace std;


int main (){
	
	
	// initialisation du tableau fréquence
	int frequences[128];
	for (int i =0; i<128; i++){
		frequences[i] = 0;
	}


	string phrase = "concombre";
	charFrequences(phrase, frequences);
	display(frequences, 128);

	/////////// TEST DE HUFMANHEAP
	HuffmanHeap* test = new HuffmanHeap;
	huffmanHeap(frequences, test);
	test->printHeap();
	return 0;
}
