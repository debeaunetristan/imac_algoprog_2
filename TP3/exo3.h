#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void display(int* tab, int taille);

struct binTree {
    int val;
    binTree* leftTree = nullptr;
    binTree* rightTree = nullptr;

    bool estFeuille(){
        return (this->leftTree == nullptr && this->rightTree == nullptr);
    }

    bool estIncomplet(){
        return (this->leftTree == nullptr || this->rightTree == nullptr);
    }
    
    
    
    void insertNumber(int value){
        
        // on crée le nouvel arbre qui contient la valeur value
        binTree* newFeuille = new binTree;
        newFeuille->val = value;

            // on compare avec la feuille pour l'insérer à gauche ou à droite
            if (value < this->val) { // Gauche
                if (this->leftTree == nullptr) {
                    this->leftTree = newFeuille;
                }
                else {
                    (this->leftTree)->insertNumber(value);
                }
            }

        // on est dans le cas où on a pas une feuille, suivant la comparaison on descend récusrsivement à gauche ou à droite
        else {
            if (this->rightTree == nullptr) {
                this->rightTree = newFeuille;
            }
            else {
                (this->rightTree)->insertNumber(value);
            }
        }
    }

    void print(){
        cout << "Noeud " << this->val << endl;
        //cout << "   ";
        

        if (this->leftTree != nullptr){
            cout << " Left de " << this->val << " est : " << this->leftTree->val << endl;
            this->leftTree->print();
        }
        
        if (this->rightTree != nullptr){
            cout << " Right de " << this->val << " est : " << this->rightTree->val << endl;
            this->rightTree->print();
        }

        if (this->estIncomplet()){
            cout << "/////////" << endl;
        }
    }

    int height() {
        // init : l'arbre vide est de taille nul
        if (this== nullptr){
            return 0;
        }

        // récursif : 1 -> passage sur un noeud ; max() -> hauteur est le max de celle de gauche et celle de droite
        return 1+std::max(this->leftTree->height(),this->rightTree->height());
    }

    int nodeCount() {
        // init : l'arbre vide a 0 noeuds
        if (this== nullptr){
            return 0;
        }

        // récursif : 1 -> passage sur un noeud + ceux de gauche et ceux de droite
        return 1+this->leftTree->nodeCount()+this->rightTree->nodeCount();
    }

    int allLeaves(binTree* leaves[], int* leavesCount) {
        // si c'est une feuille on remplit
        if (this->estFeuille()) {
            cout << "Feuille : " << this->val << endl;
            *leavesCount+=1;
            leaves[*leavesCount]=this;
        }
        
        // on descend récursivement dans les branches non vides
        if (this->rightTree != nullptr) {
            this->rightTree->allLeaves(leaves, leavesCount);
        }
        if (this->leftTree != nullptr) {
            this->leftTree->allLeaves(leaves, leavesCount);
        }
    return *leavesCount;
    }

    //enfant gauche, noeud, enfant droit
    int inorderTravel(binTree* nodes[], int* nodesCount) {

        // enfant gauche
        if (this->leftTree != nullptr) {
            this->leftTree->inorderTravel(nodes, nodesCount);
        }

        // noeud
        nodes[*nodesCount]=this;
        *nodesCount+=1;

        //enfant droit
        if (this->rightTree != nullptr) {
            this->rightTree->inorderTravel(nodes, nodesCount);
        }

        return *nodesCount;
    }

    // noeud, enfant gauche, enfant droit
    int preorderTravel(binTree* nodes[], int* nodesCount) {

        // noeud
        nodes[*nodesCount]=this;
        *nodesCount+=1;

        
        // enfant gauche
        if (this->leftTree != nullptr) {
            this->leftTree->inorderTravel(nodes, nodesCount);
        }

        //enfant droit
        if (this->rightTree != nullptr) {
            this->rightTree->inorderTravel(nodes, nodesCount);
        }

         return *nodesCount;
    }

    // enfant gauche, enfant droit, noeud
    int postorderTravel(binTree* nodes[], int* nodesCount) {
        
        // enfant gauche
        if (this->leftTree != nullptr) {
            this->leftTree->inorderTravel(nodes, nodesCount);
        }

        //enfant droit
        if (this->rightTree != nullptr) {
            this->rightTree->inorderTravel(nodes, nodesCount);
        }

        // noeud
        nodes[*nodesCount]=this;
        *nodesCount+=1;

        return *nodesCount;
    }

    binTree* search(int value) { 
        // le noeud est bon, on le renvoie
        if (this->val == value) {
            return this;
        }

       // à gauche si la valeur est inf au noeud
        if (value < this->val) {
            return this->leftTree->search(value);
        }

        // sinon elle est supp au noeud donc à droite
        else {
            return this->rightTree->search(value);
        }
        
        // s'il n'est pas trouvé on renvoie un arbre vide
        binTree* ArbreVide = new binTree;
        return ArbreVide;
    
    }


};



// AFFICHAGE
void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}