#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void display(int* tab, int taille);
int binarySearch(int* tab, int taille, int toSearch);
void binarySearchAll(int* tab, int taille, int toSearch, int* indexMin, int* indexMax);


int main(){
    
    int test[5] = {2,7,7,7,8};
	int test2[5] = {1,2,3,4,5};
	int test3[3] = {7,7,8};
	int test4[6] = {0,4,6,6,8,8};
	int min = 0;
	int max = 0;

	// Test de binarySearchAll
	binarySearchAll(test2, 5,7,&min,&max);
	display(test2,5);
	cout << "min : " << min << " // max : " << max << endl;

	min = 0;
	max = 0;
	binarySearchAll(test3, 3,7,&min,&max);
	display(test,5);
	cout << "min : " << min << " // max : " << max << endl;

	min = 0;
	max = 0;	
	binarySearchAll(test4, 6,7,&min,&max);
	display(test,5);
	cout << "min : " << min << " // max : " << max << endl;
    
    
}
// FONCTION BINARYSEARCH
int binarySearch(int* tab, int taille, int toSearch){
	
	// initialisation des valeurs
	int start = 0;
	int end = taille;
	int mid = 0;

	// tant qu'on a pas juste un élément on peut couper
	while (start < end){
		mid = (start+end)/2;
		
		// si toSearch > mid -> on cherche à droite
		if(toSearch>tab[mid]){
			start = mid+1;
		}
		else if (toSearch < tab[mid])// si toSearch < mid -> on cherche à gauche
		{
			end = mid;
		}
		else{
			return mid; // on est sur la valeur on la renvoie
		}
	}
	return -1;
}


// FONCTION BINARYSEARCH
void binarySearchAll(int* tab, int taille, int toSearch, int* indexMin, int* indexMax){
	// tableau vide ->po possible
	if (taille==0) {
		*indexMin= -1;
		*indexMax= -1;
		return;
	}

	// on récup déjà l'index min
	*indexMin = binarySearch(tab, taille, toSearch);
	*indexMax = *indexMin;


	// on avance jusqu'à ne plus le trouver
	for (int a= *indexMin+1; a<taille ; a++){
		if (tab[a] != toSearch) return;
		*indexMax = a+1;
	}
}



// AFFICHAGE
void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}