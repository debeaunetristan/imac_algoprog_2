#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void display(int* tab, int taille);
int binarySearch(int* tab, int taille, int toSearch);


int main(){
    
    int test[6] = {2,4,7,8,9,123};
    display(test,6);

	// Test de binarySearch
	cout << binarySearch(test, 6, 8) << endl;
	cout << binarySearch(test, 6, 12) << endl;
	cout << binarySearch(test, 6, 123) << endl;
	cout << binarySearch(test, 6, 4) << endl;
    

    
    // display(test,6);
}

// FONCTION BINARYSEARCH
int binarySearch(int* tab, int taille, int toSearch){
	int start = 0;
	int end = taille;
	int mid = 0;

	while (start < end){
		mid = (start+end)/2;
		if(toSearch>tab[mid]){
			start = mid+1;
		}
		else if (toSearch < tab[mid])
		{
			end = mid;
		}
		else{
			return mid;
		}
	}
	return -1;
}



// AFFICHAGE
void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}