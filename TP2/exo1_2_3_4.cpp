#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void triSelection (int* tab, int taille);
void triInsertion (int* tab, int taille);
void triBulle(int* tab, int taille);
void triRapide(int* tab, int taille);


void display(int* tab, int taille);

int main(){
    
    int test[6] = {12,6,5,7,3,4};
    display(test,6);
    
    ///////////////// test tri selection
    //triSelection(test,6);

    ///////////////// test tri insertion
    //triInsertion(test,6);


    ///////////////// test tri bulle
    //triBulle(test,6);

    ///////////////// test tri insertion
    triRapide(test,6);    
    
    
    display(test,6);
}






// FONCTION POUR TRI SELECTION
void triSelection (int* tab, int taille){
    int min = tab[0];
    int indexMin = 0;
    for(int i = 0; i<taille; i++){
        min = tab[i]; // réinit au début de la recherche
        indexMin = i; // réinit au début de la recherche
        for (int j = i+1; j<taille; j++){
            if (tab[j] < min){ // le min à partir de i
                min = tab[j]; // update du min
                indexMin = j;
            }
        }
        tab[indexMin] = tab[i]; // changement dans la liste
        tab[i] = min;
    }
}




// FONCTION POUR TRI INSERTION
void triInsertion (int* tab, int taille){
    
    int result[taille]; // init du tableau result
    int tailleRes = 1;
    bool dansIf = false;
    result[0] = tab[0];

    for (int n = 1; n<taille ; n++){ // parcour de la liste de base
        dansIf = false;
        
        for (int m = 0; m< tailleRes; m++){ // parcour pour la comparaison dans le nouveau tableau
            if (tab[n] < result[m]){
                dansIf = true;
                for (int k = tailleRes; k>m; k--){ // parcour pour l'insertion à la bonne place
                    result[k] = result[k-1]; // décalage des éléments au dessus
                }
                result[m] = tab[n]; // insertion de l'élément à la place voulue
                break;
            }
        }
        
        tailleRes ++; // augmentation du nb d'éléments insérés
        
        if (!dansIf) { // on est pas allé dans le if -> doit etre insérer à la fin
                result[tailleRes-1] = tab[n];
            }
    
    }
    
    for (int i = 0; i<taille; i++){ // realoue dans tab
        tab [i] = result [i];
    }
}


// FONCTION POUR LE TRI A BULLE
void triBulle(int* tab, int taille){
    //int result[taille]; // init du tableau result
    int interVal = 0;  // valeur intermédiaire afin de faire l'échange
    bool swap = false;
    for(int j = 0; j<taille; j++){ // on effectue le processus au moins
        swap = false;
        for (int i = 0; i<taille-1;i++){
            if (tab[i+1] < tab[i]) { // cas de non tri
                swap = true;
                interVal = tab[i]; // j'effectue swap
                tab[i] = tab[i+1];
                tab[i+1] = interVal;
            }
        }

    if(!swap) return; // Si on a pas swap => c'est trié donc fin de fonction   
    }
}

// FONCTION POUR LE TRI RAPIDE
void triRapide(int* tab, int taille){
    if (taille <= 1) return;
    
    // choix du pivot
    int pivot = tab[0];
    
    // init taille petit et grand
    int taillePetit = 0;
    int tailleGrand = 0;

     // initialisation des sous listes
    int Lpetit[taille];
    int Lgrand[taille];

    
    for (int i = 1; i< taille; i++){ // détermine taille des tableaux petit et grand
        if (tab[i]<pivot) {
            Lpetit[taillePetit] = tab[i];
            taillePetit ++;
        }else{
            Lgrand[tailleGrand] = tab[i];
            tailleGrand ++;
        }
    }
    
    // la récursivité !!
    triRapide(Lpetit, taillePetit);
    triRapide(Lgrand, tailleGrand);

    display(Lpetit, taillePetit);
    display(Lgrand, tailleGrand);

    
    // la partie réunion
    for(int i = 0; i < taillePetit; i++){ //insertion du petit
        tab[i] = Lpetit[i];
    }
    
    tab[taillePetit] = pivot; // insertion du pivot
    
    for(int i = 0; i < tailleGrand; i++){ //insertion du grand
        tab[taillePetit+i+1] = Lgrand[i];
    }
}





// AFFICHAGE
void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}