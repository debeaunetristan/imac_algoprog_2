#include <iostream>
using namespace std;

void display(int* tab, int n);
int* merge(int* t1, int n1, int* t2, int n2);
int* merge_sort(int* tab, int n);


int main() {
    
    // test du merge sort
    int tab_test[7] = {12,4,8,74,30,21,0};
    display(tab_test, 7);
    
    
    display(merge_sort(tab_test, 7), 7); 
}



// AFFICHAGE
void display(int* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}





int* merge(int* t1, int n1, int* t2, int n2) {
    int i = 0;
    int j = 0;

    int* tab = new int; //creation de tab
    int k = 0; // taille de tab

    // on parcourt t1 et t2
    while (i<n1 && j<n2) { 
        if (t1[i] < t2[j]) { //comparaison entre le plus petit entre t1 et t2
            tab[k] = t1[i]; //qu'on ajoute à tab
            i++;
            k++;
        }

        else {
            tab[k] = t2[j];
            j++;
            k++;
        }
    }

    for (int i1=i; i1<n1; i1++) { // concaténation de t1 à partir de i
        tab[k] = t1[i1];
        k++;
    }

    for (int j1=j; j1<n2; j1++) { // concaténation de t2 à partir de j
        tab[k] = t2[j1];
        k++;
    }
    return tab;
}

int* merge_sort(int* tab, int n) {

    // si tab a 1 ou 0 element, est deje trié
    if (n <= 1) {
        return tab;
    }

    else {
        int n2 = n/2;
        
        // on coupe la poire en deux
        int t1[n2];
        int t2[n-n2];

        for (int i=0; i<n2; i++) {
            t1[i]=tab[i];
        }

        for (int i=n2; i<n; i++) {
            t2[i-n2]=tab[i];
        }
        return merge(merge_sort(t1, n2), n2, merge_sort(t2, n-n2), n-n2);
    }
}





