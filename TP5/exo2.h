#include <iostream>
using namespace std;

int hashPoly (string key);
void display(string* tab, int taille);


int hashPoly (string key, int size){
    return (int) key[0] % size;
}




struct map {
    string chaine;
    int val;
    
    map* leftTree;
    map* rightTree;
    
    bool estIncomplet(){
        return (this->leftTree == nullptr || this->rightTree == nullptr);
    }


    int hashPoly(string word){
        int res = 0;
        int puiss = 1;
        for(int i=word.size()-1; i>=0; i--){
            res+= (int) word[i]*puiss;
            puiss *=256;
        }
        return res;
    }


    void print(){
        cout << "Noeud  :  Mot -> " << this->chaine << " // value -> " << this->val << " // hash code -> " << this->hashPoly(this->chaine) << endl;        

        if (this->leftTree != nullptr){
            cout << " Left de " << this->chaine << " est : " << this->leftTree->chaine << endl;
            this->leftTree->print();
        }
        
        if (this->rightTree != nullptr){
            cout << " Right de " << this->chaine << " est : " << this->rightTree->chaine << endl;
            this->rightTree->print();
        }

        if (this->estIncomplet()){
            cout << "/////////" << endl;
        }
    }


    
    // fonction issue du TP3
    void insertWord(string word, int value){
        
        // on crée le nouvel arbre qui contient la valeur value
        map* newFeuille = new map;
        newFeuille->chaine = word;
        newFeuille->val = value;

        // on compare avec la feuille pour l'insérer à gauche ou à droite
        if (this->hashPoly(word) < this->hashPoly(this->chaine)) { // Gauche
            if (this->leftTree == nullptr) {
                this->leftTree = newFeuille;
            }
            else {
                (this->leftTree)->insertWord(word, value);
            }
        }

        // on est dans le cas où on a pas une feuille, suivant la comparaison on descend récusrsivement à gauche ou à droite
        else {
            if (this->rightTree == nullptr) {
                this->rightTree = newFeuille;
            }
            else {
                (this->rightTree)->insertWord(word, value);
            }
        }
    }




};


// AFFICHAGE
void display(string* tab, int taille){
    for (int i = 0; i < taille; i++){
        cout << tab[i] << "/";
    }
    cout << endl;
}
